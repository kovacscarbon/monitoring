DOMAIN?=homelab.local
SC?=local-path

install_monitoring_stack:
	helm upgrade --install loki grafana/loki-stack \
		--set grafana.ingress.hosts[0]=${DOMAIN} \
		--set grafana.persistence.storageClassName=${SC} \
		--values ./values.yaml --namespace monitoring \
		--create-namespace